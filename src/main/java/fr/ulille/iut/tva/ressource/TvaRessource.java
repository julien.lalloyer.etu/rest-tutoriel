package fr.ulille.iut.tva.ressource;

import fr.ulille.iut.tva.dto.Detaildto;
import fr.ulille.iut.tva.dto.InfoTauxDto;
import java.util.*;
import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

/**
 * TvaRessource
 */

@Path("tva")
public class TvaRessource {
    private CalculTva calculTva = new CalculTva();

    @GET
    @Path("tauxpardefaut")
    public double getValeurTauxParDefaut() {
        return TauxTva.NORMAL.taux;
    }
    
    @GET
    @Path("valeur/{niveauTva}")
    public double getValeurTaux(@PathParam("niveauTva") String niveau) {
        try {
            return TauxTva.valueOf(niveau.toUpperCase()).taux; 
        }
        catch ( Exception ex ) {
            throw new NiveauTvaInexistantException();
        }
    }
    
    @GET
    @Path("/{niveauTva}")
    public double getMontantTotal(@QueryParam("somme") double somme, @PathParam("niveauTva") String niveau) {
        try {
            return somme + (somme *  (TauxTva.valueOf(niveau.toUpperCase()).taux / 100));
        } catch (Exception ex) {
            throw new NiveauTvaInexistantException();
        }
    }
   
    @GET
    @Path("lestaux")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<InfoTauxDto> getInfoTaux() {
      ArrayList<InfoTauxDto> result = new ArrayList<InfoTauxDto>();
      for ( TauxTva t : TauxTva.values() ) {
        result.add(new InfoTauxDto(t.name(), t.taux));
      }
      return result;
    }

    @GET
    @Path("details/{taux}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Detaildto> getDetail(@PathParam("taux") String niveau,@QueryParam("somme") int somme) {
    	double tauxV = TauxTva.valueOf(niveau.toUpperCase()).taux;
    	String taux = niveau.toUpperCase();
    	double montantTva = (somme*(tauxV/100));
    	double montantTotal = somme + montantTva;
    	List<Detaildto> result = new ArrayList<>();
    	result.add(new Detaildto(taux, tauxV, montantTotal, montantTva, somme));
    	return result;
    }

   

}




