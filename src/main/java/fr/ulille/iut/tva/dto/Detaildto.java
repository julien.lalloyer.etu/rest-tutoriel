package fr.ulille.iut.tva.dto;

import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Detaildto {
	 private double montantTT;
	    private double montantTva;
	    private double somme;
	    private String taux;
	    private double tauxV;
	    
	    public Detaildto() {}
	    
	    public Detaildto(String taux, double tauxV, double montantTT, double montantTva, double somme) {
	        this.montantTT = montantTT;
	        this.taux = taux;
	        this.montantTva = montantTva;
	        this.somme = somme;
	        this.tauxV = tauxV;
	    }

		public double getMontantTT() {
			return montantTT;
		}

		public void setMontantTT(double montantTT) {
			this.montantTT = montantTT;
		}

		public double getMontantTva() {
			return montantTva;
		}

		public void setMontantTva(double montantTva) {
			this.montantTva = montantTva;
		}

		public double getSomme() {
			return somme;
		}

		public void setSomme(double somme) {
			this.somme = somme;
		}

		public String getTaux() {
			return taux;
		}

		public void setTaux(String taux) {
			this.taux = taux;
		}

		public double getTauxV() {
			return tauxV;
		}

		public void setTauxV(double tauxV) {
			this.tauxV = tauxV;
		}
	    

	  
	    
}

